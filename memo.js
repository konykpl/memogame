/**
 * Created by Pawel on 16-09-2017.
 */


let memo = (function(){

    let size = 150;
    let main = document.getElementsByClassName('memo')[0];
    main.style.width = 750 + "px";
    var img = main.children;
    let imgSrc = new Array();
    var buffer = new Array();
    let countClicked = 0;
    let point = 0;

    let imgTotal = img.length;
    let kafelek  = new Image();
    kafelek.src = "kafelek.jpeg";


    let setSize = function (size) {
        for(let i = 0; i<imgTotal; i++){
            img[i].height = size;
            img[i].width = size;
            imgSrc.push(img[i].src);
            img[i].src= kafelek.src ;
        }
    }(size);


    let shuffle = function () {
        // napelnij tablice od 0 do ilosci zdjec
        let tab = new Array();
        let i = 0;
        while (i < imgTotal) {
            tab.push(i);
            i++;
        }

        // Wylosuj liczbe i znien liczby dla tablicy
        for (let i = imgTotal; i; i--) {
            let j = Math.floor(Math.random() * i);
            [tab[i - 1], tab[j]] = [tab[j], tab[i - 1]];
        }

        return {
            tab : tab,
        }
    };

    for(let i = 0; i< imgTotal; i++){
        img[i].addEventListener("click", function () {
            switchPhoto(img[i],i);
        }.bind(this));
    }

    changePhoto = function (buffer) {
        img[buffer[1]].src = kafelek.src;

        img[buffer[3]].src = kafelek.src;
    };

    let switchPhoto = function (imag,i) {
        countClicked++;
        buffer.push(imag,i);

        imag.src = imgSrc[i];
        if(countClicked == 2){

            let b = buffer[0].src === buffer[2].src;
            if(!b){
                setTimeout(changePhoto.bind(null,buffer),1000);
            }
            if(b){
                point++;
                setPoints();
            }

            // Czyść tablice
            buffer = [];
            countClicked = 0;
        }

    };

    let setPoints = function () {
        let points = document.getElementById('points');
        points.textContent = point;
        let total = parseInt(points.textContent);
        if(total == imgTotal/2){
            alert("Gratulacje wygrałęś/aś");
        }

    };

    let init = function () {
        let tab = shuffle();

    };

    return{
        init : init,
    }

}());

memo.init();